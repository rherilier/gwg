
/* Copyright @ 2023 Rémi Hérilier
 *
 * SPDX-License-Identifier: WTFPL
 */

#include <gwg/markov_chain_generator.h>

#include <cstdio>
#include <cstring>

int main(int argc, char** argv)
{
  setlocale(LC_ALL, "");

  {
    const char *l = setlocale(LC_ALL, nullptr);

    if ((strcmp(l, "C") == 0)) {
      printf("current locale is \"%s\", please set LC_ALL to a multibyte locale\n", l);

      return 1;
    }
  }

  if (argc != 4) {
    printf("usage: %s in-symbols-file in-vocabulary-file out-generation-file\n", argv[0]);

    return 1;
  }

  gwg::markov_chain_generator mcg;

  if (mcg.load_symbols(argv[1]) == false) {
    printf("error: symbols-file \"%s\" does not seem to be valid\n", argv[1]);

    return 1;
  }

  if (mcg.load_vocabulary(argv[2]) == false) {
    printf("error: vocabylary-file \"%s\" does not seem to be valid\n", argv[2]);

    return 1;
  }

  if (mcg.save(argv[3]) == false) {
    printf("error: while writing generation-file \"%s\"\n", argv[3]);

    return 1;
  }

  return 0;
}
