
/* Copyright @ 2023 Rémi Hérilier
 *
 * SPDX-License-Identifier: WTFPL
 */

#include <gwg/markov_chain_generator.h>

#include <cstring>

#include <sys/times.h>

int main(int argc, char** argv)
{
  setlocale(LC_ALL, "");

  {
    const char *l = setlocale(LC_ALL, nullptr);

    if ((strcmp(l, "C") == 0)) {
      printf("current locale is \"%s\", please set LC_ALL to a multibyte locale\n", l);

      return 1;
    }
  }

  size_t count = 10;
  int seed = times(nullptr);

  if (argc == 3) {
    count = std::strtoul(argv[2], nullptr, 10);
  } else if (argc == 4) {
    count = std::strtoul(argv[2], nullptr, 10);
    seed = std::atoi(argv[3]);
  }

  srand(seed);
  printf("seed = %d\n", seed);

  gwg::markov_chain_generator mcg;

  if (mcg.load(argv[1]) == false) {
    printf("error: in-generation-file \"%s\" does not seem to be valid\n", argv[1]);

    return 1;
  }

  std::string result;

  while (count--) {
    mcg.generate(result);

    printf("%s\n", result.c_str());
  }

  return 0;
}
