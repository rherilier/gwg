
/* Copyright @ 2023 Rémi Hérilier
 *
 * SPDX-License-Identifier: WTFPL
 */

#include <gwg/index_trie.h>

#include <algorithm>

#include <wchar.h>

gwg::index_trie::index_trie(size_t index)
  : _index(index)
{}

bool gwg::index_trie::insert(const std::string& text, size_t index)
{
  mbstate_t mbs = {};

  return insert(mbs, std::string_view(text), index);
}

size_t gwg::index_trie::find_prefix(const std::string_view& text, size_t& length) const
{
  mbstate_t mbs = {};

  length = 0;

  return find_prefix(mbs, text, length);
}

void gwg::index_trie::dump() const
{
  dump(0);
}

bool gwg::index_trie::insert(mbstate_t& mbs, const std::string_view& word, size_t index)
{
  size_t size = word.size() + 1;

  if (size == 1) {
    _children.insert(std::make_pair("", index_trie(index)));

    return true;
  }

  size_t len = std::mbrlen(word.data(), size, &mbs);

  if (len == MBR_ERR) {
    fprintf(stderr, "error: mbrlen(\"%s\") fails with invalid or incomplete multibyte character\n",
            word.data());

    return false;
  }

  const std::string_view sym(word.data(), len);

  auto it = std::find_if(_children.begin(), _children.end(),
                         [&](const auto& value) {
                           return std::string_view(value.first) == sym;
                         });

  if (it != _children.end()) {
    return it->second.insert(mbs, word.substr(len), index);
  } else {
    auto it2 = _children.insert(std::make_pair(std::string(sym), index_trie()));

    return it2.first->second.insert(mbs, word.substr(len), index);
  }
}

size_t gwg::index_trie::find_prefix(mbstate_t& mbs, const std::string_view& word, size_t& length) const
{
  errno = 0;

  size_t len = std::mbrlen(word.data(), word.size(), &mbs);

  if (len == MBR_ERR) {
    fprintf(stderr, "error: mbrlen(\"%s\") fails with invalid or incomplete multibyte character\n",
            word.data());

    return NONE;
  }

  if (len == MBR_INSUF) {
    fprintf(stderr, "error: mbrlen(\"%s\") fails with insufficient characters\n", word.data());

    return NONE;
  }

  const std::string_view sym(word.data(), len);
  const auto it = std::find_if(_children.cbegin(), _children.cend(),
                               [&](const auto& value) {
                                 return std::string_view(value.first) == sym;
                               });

  if (it == _children.cend()) {
    const auto it2 = std::find_if(_children.cbegin(), _children.cend(),
                                  [&](const auto& value) {
                                    return value.first.size() == 0;
                                  });

    if (it2 == _children.cend()) {
      return NONE;
    } else {
      return it2->second._index;
    }
  }

  ++length;

  return it->second.find_prefix(mbs, word.substr(len), length);
}

void gwg::index_trie::dump(int level) const
{
  if (_index != NONE) {
    printf("%.*s", level * 2, "                                  ");
    printf("%zu\n", _index);
  } else {
    for (const auto& c : _children) {
      printf("%.*s", level * 2, "                                  ");
      if (c.first.size() != 0) {
        printf("'%s'\n", c.first.c_str());
      } else {
        printf("'_'\n");
      }
      c.second.dump(level + 1);
    }
  }
}
