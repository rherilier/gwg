
/* Copyright @ 2023 Rémi Hérilier
 *
 * SPDX-License-Identifier: WTFPL
 */

#include <gwg/markov_chain_generator.h>

#include <algorithm>
#include <fstream>
#include <sstream>

gwg::markov_chain_generator::markov_chain_generator()
  : _min_symbols(std::numeric_limits<size_t>::max()),
    _max_symbols(0)
{}

void gwg::markov_chain_generator::clear()
{
  _symbols.clear();
  _totals.clear();
  _counts.clear();

  _min_symbols = std::numeric_limits<size_t>::max();
  _max_symbols = 0;
}

bool gwg::markov_chain_generator::load_symbols(const std::string& filename)
{
  std::ifstream ifs(filename);

  if (ifs.is_open() == false) {
    fprintf(stderr, "not readable?!\n");

    return  false;
  }

  std::set<symbol> syms;
  std::string line;

  while (std::getline(ifs, line)) {
    // got from https://stackoverflow.com/questions/14233065/remove-whitespace-in-stdstring
    line.erase(std::remove_if(line.begin(), line.end(),
                              [](char c) {
                                return std::isspace<char>(c, std::locale::classic());
                              }),
               line.end());

    if (line.size() == 0) {
      // skip empty lines
      continue;
    }

#if __cplusplus >= 202002L
    if (line.starts_with("#")) {
#else
    if (line[0] == '#') {
#endif
      // skip comments
      continue;
    }

    syms.insert(symbol(line));
  }

  // convert a set into a vector (I forgot to keep the SO link...)
  std::move(syms.begin(), syms.end(), std::inserter(_symbols, _symbols.end()));

  for (size_t i = 0; i < _symbols.size(); ++i) {
    _trie.insert(_symbols[i].value(), i);
  }

  resize_internals(_symbols.size());

  return true;
}

bool gwg::markov_chain_generator::load_vocabulary(const std::string& filename)
{
  std::ifstream ifs(filename);
  std::string word;
  std::string_view symbol;

  while (std::getline(ifs, word)) {
    if (word.size() == 0) {
      // skip empty lines
      continue;
    }

#if __cplusplus >= 202002L
    if (word.starts_with("#")) {
#else
    if (word[0] == '#') {
#endif
      // skip comments
      continue;
    }

    symbol = std::string_view(word);

    size_t word_len = word.size();
    size_t index = NONE;
    size_t nsymbol = 0;

    do {
      size_t symbol_len = 0;
      size_t next = _trie.find_prefix(symbol, symbol_len);

      if (next == index_trie::NONE) {
        fprintf(stderr, "error with word '%s' while processing '%s'\n", word.c_str(), symbol.data());

        return false;
      }

      if (index != NONE) {
        ++_counts[index][next];
        ++_totals[index];
      }

      index = next;
      symbol = symbol.substr(symbol_len);
      word_len -= symbol_len;

      ++nsymbol;
    } while (word_len != 0);

    _min_symbols = std::min(_min_symbols, nsymbol);
    _max_symbols = std::max(_max_symbols, nsymbol);
  }

  return true;
}

bool gwg::markov_chain_generator::save(const std::string& filename)
{
  std::ofstream ofs(filename);
  size_t version_number = 1;

  ofs << version_number << std::endl;
  ofs << _symbols.size() << " " << _min_symbols << " " << _max_symbols << std::endl;

  for (size_t i = 0; i < _symbols.size(); ++i) {
    ofs << _symbols[i].to_string();

    for (const auto& count : _counts[i]) {
      ofs << " " << count;
    }

    ofs << std::endl;
  }

  return true;
}

bool gwg::markov_chain_generator::load(const std::string& filename)
{
  clear();

  std::ifstream ifs(filename);
  std::string line;

  if (not std::getline(ifs, line)) {
    fprintf(stderr, "can not read a line?!\n");

    return false;
  }

  size_t version_number = 0;

  if (sscanf(line.c_str(), "%zu", &version_number) != 1) {
    fprintf(stderr, "'%s' should contain 1 unsigned integer\n", line.c_str());

    return false;
  }

  if (version_number != 1) {
    fprintf(stderr, "version should be 1, not '%zu'\n", version_number);

    return false;
  }

  if (!std::getline(ifs, line)) {
    fprintf(stderr, "can not read a line?!\n");

    return false;
  }

  size_t nsymbols;

  if (sscanf(line.c_str(), "%zu%zu%zu", &nsymbols, &_min_symbols, &_max_symbols) != 3) {
    fprintf(stderr, "'%s' should contain 3 unsigned integers\n", line.c_str());

    return false;
  }

  resize_internals(nsymbols);

  std::string text;
  size_t value;
  size_t i = 0;

   while (std::getline(ifs, line)) {
    std::stringstream sstream(line);
    size_t j = 0;
    size_t a = 0;

    sstream >> text;
    _symbols.push_back(symbol(text));

    while (!sstream.eof()) {

      sstream >> value;

      _counts[i][j] = value;
      a += value;
      ++j;
    }

    _totals[i] = a;

    ++i;
  }

  finalize();

  return true;
}

void gwg::markov_chain_generator::generate(size_t min_symbols, size_t max_symbols, std::string& word) const
{
  word.clear();

  size_t d = max_symbols - min_symbols;

  size_t i = get_initial_symbol();
  size_t nsym = min_symbols + rand() % d;

  word += _symbols[i].value();

  while (nsym) {
#if 0
    word += _symbols[i].value();

    size_t n = rand() % _totals[i];
    size_t j = 0;

    while (n >= _counts[i][j]) {
      n -= _counts[i][j];
      ++j;
    }

    if (_totals[j] == 0) {
      // forcing break because it is known to only appear at the end of a word
      break;
    }

    i = j;
#else
    //printf("word: '%s'\n", word.c_str());

    i = get_next_symbol(i, nsym);
#endif

    word += _symbols[i].value();

    --nsym;
  }

  //printf("final word: '%s'\n", word.c_str());
}

size_t gwg::markov_chain_generator::find_prefix(const std::string_view& text, size_t& length) const
{
  return _trie.find_prefix(text, length);
}

void gwg::markov_chain_generator::dump() const
{
  _trie.dump();
}

void gwg::markov_chain_generator::resize_internals(size_t n)
{
  _totals.resize(n, 0);
  _counts.resize(n);

  for (auto& it : _counts) {
    it.clear();
    it.resize(n, 0);
  }
}

void gwg::markov_chain_generator::finalize()
{
  _initials.clear();

  for (size_t i = 0; i < _symbols.size(); ++i) {
    if ((_symbols[i].mode() & symbol::NEED_BEFORE) == 0) {
      _initials.push_back(i);
    }
  }
}

size_t gwg::markov_chain_generator::get_initial_symbol() const
{
  size_t i;

  do {
    i = _initials[rand() % _initials.size()];
  } while (_totals[i] == 0);

  return i;
}

size_t gwg::markov_chain_generator::get_next_symbol(size_t i, size_t l) const
{
  while (true) {
    size_t n = rand() % _totals[i];
    size_t j = 0;

    while (n > _counts[i][j]) {
      n -= _counts[i][j];
      ++j;
    }

    if (l == 1) {
      if (_symbols[j].mode() & symbol::NEED_AFTER) {
        // it wants an other symbol, choosing another one
        continue;
      }

      return j;
    }

    if (_totals[j] == 0) {
    // it is a statistical terminal symbol, chosing another one
      continue;
    }

    return j;
  }

  // unreachable but GCC whines
  return NONE;
}
