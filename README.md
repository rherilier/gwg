# General Word Generator

## Description

This project aims to provide a word generator using Markov chains.

As I wrote it to be initially used with Japanese words written in rōmaji (i.e using latin characters sequence as syllables), there should logically be no problem to support any kind of language or writing system.

For a sample, look in directory *test-japanese*.

It is splitted into 3 parts:
* a static library (no need for dynamic one);
* a binary (`gwg-build`) to build a generation file from symbols and vocabulary files;
* a binary (`gwg-gen`) to generate words set given a generation file (and some other parameters).

## License

This project is distributed under the WTFPL 2 or later (see LICENSE for details).

## Usage

### `gwg-build`

To produce a *generation file* named `generation-file` from a *symbols file* `symbols-files` and a  *vocabulary file* `vocabulary-file`:

```shell
$ gwg-build symbols-file vocabulary-file generation-file
```

### `gwg-gen`

By default, it generate 10 words using a *random* seed (which is displayed in the first line):

```shell
$ gwg-gen generation-file
```

But you may want more words:

```shell
$ gwg-gen generation-file count
```

You also want to keep track of the run by using a predefined seed:

```shell
$ gwg-gen generation-file count seed
```

## File formats specification

### Symbols file

It is defined as UTF-8 text file (with no specific extension) which matchs the following rules:

* UNIX newline;
* empty lines are ignored;
* lines starting with `#` are ignored (use it as comments);
* one symbol (a characters sequence) per line;
* symbol with a leading dash (`-`) can not be used as a starting point;
* symbol with a leading dot (`.`) can not be used as a starting or ending point;
* there is no constraint about order.

The characters `+`, `^`, and `$` are reserved.

### Vocabulary file

It is defined as UTF-8 text file (with no specific extension) which matchs the following rules:

* UNIX newline;
* empty lines are ignored;
* lines starting with `#` are ignored (use it as comments);
* one word per line;
* there is no constraint about order.

### Generation file

It is defined as UTF-8 text file (with no specific extension) which uses the following format:

* UNIX newline;
* a line with 1 unsigned integer used as a version number;
* a line with 3 unsigned integers standing for:
  * the symbol count;
  * the minimal and maximal counts of syllables in words from the source vocabulary file.
* a line for each symbol containing the symbol (as defined in the source symbols file) followed by a vector of integers corresponding to the symbol's Makov chain's transition vector.
