#!/usr/bin/env python3

import wikipediaapi
import re

ascii_rewriter = {ord('ā'):'aa', ord('ē'):'ee', ord('ī'):'ii', ord('ō'):'oo', ord('ū'):'uu'}

def get_by_category(cnx, tags, filename):

    result = []

    for tag in tags:
        cat = cnx.page(tag)

        for value in cat.categorymembers.values():
            text = value.title.translate(ascii_rewriter)

            if not text.isascii():
                continue

            if re.search(":", text):
                continue

            name = re.sub(" .*$", "", text.lower())

            if name == "coda":
                # it appears in surnames list
                continue

            result.append(name)

    result = sorted(list(set(result)))

    print(f"have found {len(result)} entries")

    with open(filename, "w") as output:
        print(f"writing in {filename}")
        for value in result:
            print(f"{value}", file=output)


cnx = wikipediaapi.Wikipedia('Get japanese {given,sur}name (rherilier@yahoo.fr)', 'en')

get_by_category(cnx, ["Category:Japanese-language surnames"], "japanese-surnames.txt")
get_by_category(cnx, ["Category:Japanese unisex given names", "Category:Japanese feminine given names"], "japanese-feminine-givennames.txt")
get_by_category(cnx, ["Category:Japanese unisex given names", "Category:Japanese masculine given names"], "japanese-masculine-givennames.txt")

