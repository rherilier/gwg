
/* Copyright @ 2023 Rémi Hérilier
 *
 * SPDX-License-Identifier: WTFPL
 */

#ifndef GWG_SYMBOL
#define GWG_SYMBOL

#include <string>
#include <string_view>

#include <stdexcept>

namespace gwg
{

class symbol
{
public:
  enum mode_e : int {
    NONE        = 0,
    NEED_AFTER  = 1,
    NEED_BEFORE = 2,
    NEED_AROUND = 3
  };

public:
  template <typename T>
  explicit symbol(const T& value)
    : _mode(deduce_mode(value))
  {
    switch (_mode) {
    case NONE:
      _value = value;
      break;
    default:
      _value = value.substr(1);
      break;
    }
  }

public:
  const std::string& value() const { return _value; }

  mode_e mode() const { return _mode; }

public:
  std::string to_string() const
  {
    return to_string(_mode) + _value;
  }

private:
  template <typename T>
  static mode_e deduce_mode(const T& value)
  {
    switch (value[0]) {
    case '.':
      return NEED_AROUND;
    case '-':
      return NEED_BEFORE;
    case '+':
    case '^':
    case '$':
      throw std::invalid_argument("[+^$] are reserved O:-.");
    default:
      return NONE;
    }
  }

  static std::string to_string(mode_e mode)
  {
    switch (mode) {
    case NEED_AROUND:
      return std::string(".");
    case NEED_AFTER:
      return std::string("+");
    case NEED_BEFORE:
      return std::string("-");
    default:
      return std::string();
    }
  }

private:
  std::string _value;
  mode_e _mode;
};

}

namespace std
{

  template<>
  struct less<gwg::symbol>
  {
    bool operator()(const gwg::symbol& lhs, const gwg::symbol& rhs) const
    {
      return lhs.value() < rhs.value();
    }
  };

}

#endif // GWG_SYMBOL
