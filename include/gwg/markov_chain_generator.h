
/* Copyright @ 2023 Rémi Hérilier
 *
 * SPDX-License-Identifier: WTFPL
 */

#ifndef GWG_MARKOV_CHAIN_GENERATOR
#define GWG_MARKOV_CHAIN_GENERATOR

#include <gwg/index_trie.h>
#include <gwg/symbol.h>

#include <limits>
#include <set>
#include <string>
#include <string_view>
#include <unordered_map>
#include <vector>

#include <cstring>

namespace gwg
{

class markov_chain_generator
{
public:
  static const constexpr size_t NONE = std::numeric_limits<size_t>::max();

public:
  using symbols_t = std::vector<symbol>;
  using sizes_t = std::vector<size_t>;
  using counts_t = std::vector<std::vector<size_t>>;
  using words_t = std::vector<std::string>;

public:
  markov_chain_generator();

public:
  const symbols_t& symbols() const { return _symbols; }
  const counts_t& counts() const { return _counts; }
  const sizes_t& totals() const { return _totals; }

public:
  void clear();

public:
  bool load_symbols(const std::string& filename);
  bool load_vocabulary(const std::string& filename);

public:
  bool save(const std::string& filename);
  bool load(const std::string& filename);

public:
  void generate(std::string& word) const
  {
    generate(_min_symbols, _max_symbols, word);
  }

  void generate(size_t min_symbols, size_t max_symbols, std::string& word) const;

public:
  size_t find_prefix(const char* text, size_t& length) const
  {
    return _trie.find_prefix(std::string_view(text, strlen(text)), length);
  }

  size_t find_prefix(const std::string& text, size_t& length) const
  {
    return _trie.find_prefix(std::string_view(text), length);
  }

  size_t find_prefix(const std::string_view& text, size_t& length) const;

public:
  void dump() const;

private:
  void resize_internals(size_t n);
  void finalize();

private:
  size_t get_initial_symbol() const;
  size_t get_next_symbol(size_t i, size_t l) const;

private:
  symbols_t _symbols;
  counts_t _counts;
  sizes_t _totals;
  sizes_t _initials;
  index_trie _trie;
  size_t _min_symbols;
  size_t _max_symbols;
};

}

#endif // GWG_MARKOV_CHAIN_GENERATOR
