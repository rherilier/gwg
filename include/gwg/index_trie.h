
/* Copyright @ 2023 Rémi Hérilier
 *
 * SPDX-License-Identifier: WTFPL
 */

#ifndef GWG_INDEX_TRIE
#define GWG_INDEX_TRIE

#include <gwg/symbol.h>

#include <limits>
#include <string>
#include <string_view>
#include <unordered_map>

namespace gwg
{

class index_trie
{
public:
  static const constexpr size_t NONE = std::numeric_limits<size_t>::max();
  static const constexpr size_t MBR_ERR = std::numeric_limits<size_t>::max();
  static const constexpr size_t MBR_INSUF = std::numeric_limits<size_t>::max();

public:
  explicit index_trie(size_t index = NONE);

public:

  bool insert(const std::string& text, size_t index);

public:
  size_t find_prefix(const std::string& text, size_t& length) const
  {
    return find_prefix(std::string_view(text), length);
  }

  size_t find_prefix(const std::string_view& text, size_t& length) const;

public:
  void dump() const;

protected:
  bool insert(mbstate_t& mbs, const std::string_view& word, size_t index);

protected:
  size_t find_prefix(mbstate_t& mbs, const std::string_view& word, size_t& length) const;

protected:
  void dump(int level) const;

private:
  using children_t = std::unordered_map<std::string, index_trie>;

private:
  size_t _index;
  children_t _children;
};

}

#endif // GWG_INDEX_TRIE
